#!/bin/bash

# Git config script v1.0
# Alessio Franceschelli 2012

WHERE="--global"
USER_NAME=""
USER_EMAIL=""

BCOMPARE=0

usage ()
{
	echo "Usage: $0 \"name surname\" \"email\" [--bcompare]"
	exit -1
}

while [ $# -ne 0 ]; do
	case "$1" in
		"--bcompare")
			BCOMPARE=1
		    ;;
		*)
			if [ -z "$USER_NAME" ]
			then
				USER_NAME="$1"
			else
				if [ -z "$USER_EMAIL" ]
				then
					USER_EMAIL="$1"
				else
					usage
				fi
			fi
	   ;;
	esac
	shift
done

if [ -z "$USER_NAME" ] | [ -z "$USER_EMAIL" ]
then
	usage
fi

echo "$0 \"$USER_NAME\" \"$USER_EMAIL\" --bcompare=$BCOMPARE"

git config $WHERE alias.lg "log --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit --date=relative"

git config $WHERE user.name "$USER_NAME"
git config $WHERE user.email "$USER_EMAIL"

git config $WHERE core.autocrlf false

git config $WHERE diff.renamelimit 10000

if [ $BCOMPARE -eq 1 ]
then
	git config $WHERE diff.tool bcompare
	git config $WHERE difftool.prompt false
	git config $WHERE difftool.bcompare.cmd 'C:/Program Files (x86)/Beyond Compare 3/bcomp.exe'  $LOCAL  $REMOTE
	git config $WHERE merge.tool bcompare
	git config $WHERE mergetool.bcompare.cmd 'C:/Program Files (x86)/Beyond Compare 3/bcomp.exe'  $LOCAL  $REMOTE  $BASE $MERGED
	git config $WHERE mergetool.bcompare.trustexitcode true
fi
